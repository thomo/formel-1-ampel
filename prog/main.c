//
// Processor 12F629 12F635
//

#define _XTAL_FREQ 4000000.0
#include <htc.h>

// high speed crystal
// do not protect memory
// brown-out reset disabled
// MCLR pin function disabled
// power-up timer enabled
// watchdog timer disabled

__CONFIG(INTIO & UNPROTECT & BORDIS & MCLRDIS & PWRTEN & WDTDIS & 0x31FF);
#include "global.h"

void interrupt my_isr(void)
{
    INTCON = 0x00;  // Disable all interrupts and clear flags
}

void WAIT_1SEC()
{
  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);

  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);
  	__delay_ms(100);
}
  
void WAIT_100MSEC()
{
  	__delay_ms(100);
}
  
void main(void)
{
  GIE   = 0;            // disable all interrupts
  WPUDA = 0x00;         // but disable all pull-ups

  OPTION = OPTION_CONFIG; 
  
  CMCON0 = 0x07;        // Digital I/O
  GPIO = 0xFF;
  
	TRISIO = 0b11001000;	// GP<5:4>+<2:0> output	
  
  // main loop	
  for(;;)
  {
    GPIO = 0xFF;
    INTCON = 0x00;  // Disable all interrupts and clear flags

    IOC3 = 1;
    GPIE = 1;
    
    SLEEP();
    
    LED1_ON;
    WAIT_1SEC();
    LED2_ON;    
    WAIT_1SEC();
    LED3_ON;    
    WAIT_1SEC();
    LED4_ON;    
    WAIT_1SEC();
    LED5_ON;    
    WAIT_1SEC();
    WAIT_1SEC();
    WAIT_100MSEC();
    WAIT_100MSEC();
    WAIT_100MSEC();
  }
}