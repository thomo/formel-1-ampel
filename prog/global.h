// globals

#ifndef GLOBAL_H
#define GLOBAL_H

#define bitset(var,bitno) ((var) |= (1 << (bitno)))
#define bitclr(var,bitno) ((var) &= ~(1 << (bitno)))
#define bittst(var,bitno) (var & (1 << (bitno)))

#define LOWBYTE(v)   ((unsigned char) (v))
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))

                      //0-------  global disable weak pull-ups
                      //-x------
                      //--0-----  TMR0 Source is internal clock
                      //---x----
                      //----0---  PreScaler to TMR0
                      //-----001  PreScaler = 4
#define OPTION_CONFIG 0b10000001

volatile char button_queue;

#define BUTTON_PRESSED button_queue == 0xFF

#if	defined(_12F629)
#define GP0 GPIO0
#define GP1 GPIO1
#define GP2 GPIO2
#define GP3 GPIO3
#define GP4 GPIO4
#define GP5 GPIO5
#define WPUDA WPU
#define CMCON0 CMCON
#endif

#define LED1_ON    GP2 = 0
#define LED2_ON    GP1 = 0
#define LED3_ON    GP0 = 0
#define LED4_ON    GP4 = 0
#define LED5_ON    GP5 = 0

#endif